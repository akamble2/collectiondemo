package com.agsft.collectiondemo.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.agsft.collectiondemo.model.Employee;

public class ListOperationImpl implements ListOperation {
	
	
	public void DisplayName(List<Employee> employee)
	{
		
		for(Employee e:employee)
		{
			System.out.println(e.getFirstname());
		}
		
	}

	@Override
	public void DisplaySalary(List<Employee> employee) {
		employee.stream().forEach(s-> {System.out.println(s.getSalary()); System.out.println(s.getLastname());});
		
	}

	@Override
	public List<String> getNames(List<Employee> employee) {
		List<String> names= new ArrayList<>();
		
		for(Employee e: employee)
		{
			names.add(e.getFirstname());
		}
		
		return names;
		
	}

	@Override
	public Set<Integer> getId(List<Employee> employee) {
			Set<Integer> id= new HashSet<Integer>();
		
			for(Employee e: employee)
			{
				id.add(e.getId());
			}
			
			return id;
		
	}

	@Override
	public List<Employee> deleteemployee(int id,List<Employee> employee) {
		
		if(employee.isEmpty())
		{
			return null;
		}
		
		for(Employee e: employee)
		{
			if(e.getId()==id)
			{
				employee.remove(e);
			}
		}
		
		return employee;
		
		
		
	}

}

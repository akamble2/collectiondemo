package com.agsft.collectiondemo.service;

import java.util.List;
import java.util.Set;

import com.agsft.collectiondemo.model.Employee;

public interface ListOperation {
	
	void DisplayName(List<Employee> employee);
	void DisplaySalary(List<Employee> employee);
	List<String> getNames(List<Employee> employee);
	Set<Integer> getId(List<Employee> employee);
	List<Employee> deleteemployee(int id,List<Employee> employee);
	
	
	

}
